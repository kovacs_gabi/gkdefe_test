Digital Education Frontend Test
==========================

Please follow next steps to install and run the test

#Requirements
####  Install Nodejs
Install latest version of Nodejs from their website: [https://nodejs.org/](https://nodejs.org/). If you have Ubuntu 14.04 LTS, please install from source. Need at least v0.12.0.
Make sure you also have **npm** installed after
####  Install CoffeScript
CoffeScript node parser needs to be installed globally
```sh
$ npm install -g coffee-script
```
####  Install Compass
Compass is needed for sass mixins
```sh
$ gem update --system
$ gem install compass
```

####  [Optional] Install Gulp
As an option Gulp can also be installed globally
```sh
$ npm install -g gulp
```

#Install Test Application
####  Clone test application
```
$ git clone git@bitbucket.org:kovacs_gabi/gkdefe_test.git
```
####  Make bash scripts executable
Assuming you cloned into **gkdefe_test**
```
$ cd gkdefe_test
$ chmod +x scripts/init.sh
$ chmod +x scripts/server.sh
```
#Install test application
Run the following script to install/reset the application
```
$ ./scripts/init.sh
```

#Run test application
Run the following script to start a local webserver that will automatically open a web browser on http://localhost:3000
```
$ ./scripts/server.sh
```

Access http://localhost:3001 for BrowserSync control panel. Multiple browsers can run on http://localhost:3000 at the same time and they are in sync

# Requirements included in this version of test app

* CSS Gradients where necessary and practical.
* As few image files as possible
* Usage of @font-face (these fonts being premium might not be usable, find ready made webfonts on fontsquirrel.com or use the generator there to make your own webfonts.
* Box-shadow, box-shadow with inset, text-shadow.
* Scalable design
* Reusable elements page elements
* Logo should be over the menu with z-index.
* Lightbox for contact us, usable and unobtrusive with intuitive controls.
* Commented HTML&CSS Code and well organized
* Custom scroll for the first section under the slideshow horizontal(not very usable, but for this demonstration, it should serve the purpose of showing us what you know)
* The second column should be scalable vertically
* Custom scroll for the third section under the slideshow vertical(not very usable, but for this demonstration, it should serve the purpose of showing us what you know)
* When scrolling down, the header should be smaller and remain fixed, small as in we should see only the contact, social icons, this will require JavaScript for the smaller header, but the rest should be made in CSS.
* Example usage of CSS Media Queries, print view, mobile view, example of loading other css scripts for different devices and resolutions, this can be tested locally by resizing the window, when the windows is under a number of pixes specified in the media query, a different CSS should load.
* Crossbrowser compatibily (IE9, Chrome, Firefox, Safari)
* Browser specific detection queries for webkit browser and others, conditional tags
<!--[if IE 6]> ...include IE6-specific stylesheet here... <![endif]-->
@-moz-document url-prefix() { your css code for firefox only.
}
* Webkit keyframing for the menu on page load in sequential order (see example http://css3.wdfiles.com/local--code/blog%3Aanimated-page-entry-with-css3/5 )
* Mobile styles for this page (iOS, Android[diffrent resolutions], phones and tablets), Simulate using xCode if possible and the Google Android SDK.
* The Contact button should open in a lightbox a different page with, input type: text, textarea, checkbox, dropdown, CSS Styling for the text and textarea, optional skin the checkboxes with JS, if you are using HTML4, the usage of html5 boilerplate js library is required for client-side validation of the fields, if using HTML5, use the built in validation.(ajax call) - Done 60%; No proper async validation behind the scene was done.
* Create an “acordeon” animation on the page (not using plugins, from scratch). The animation should be hover triggered
* If the browsers javascript is disabled an message should appear stating that you have to enable it to view the page
* The social buttons should be monochrome and on hover get colored
* The link main menu links should point to content pages for which you have to provide the design using elements from initial PSD(and http://www.lipsum.com/)
The first column of links in the footer should have jQuery tooltips

