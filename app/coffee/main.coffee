$ ->

  # Hide Enable JS Warning
  $('.js-enable').hide();

  # Sticky Header
  waypoint = new Waypoint(
    element: document.getElementById("js-main-menu-waypoint")
    handler: (direction) ->
      if direction == 'down'
        $('.navbar-default').addClass 'navbar-fixed-top small-header'
        $('body').addClass 'main-content-fixed'
      else if direction == 'up'
        $('.navbar-default').removeClass 'navbar-fixed-top small-header'
        $('body').removeClass 'main-content-fixed'
      return
    )

  # Custom Scroll. Cross Browser.
  $('.custom-scroll').jScrollPane()

  #jQuery Tooltip
  $('.has-tip').qtip()

  return