#!/bin/bash

rm -rf _dist
rm -rf _production
rm -rf node_modules
rm -rf bower_components
npm cache clean
bower cache clean

npm install
./node_modules/.bin/bower install
