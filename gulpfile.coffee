# Include Gulp
gulp           = require('gulp')

# Templates plugins
jade           = require('gulp-jade')

# CSS plugins
compass        = require('gulp-compass')
cssmin         = require('gulp-minify-css')
autoprefixer   = require('gulp-autoprefixer')

# JS plugins
coffee         = require('gulp-coffee')
concat         = require('gulp-concat')
uglify         = require('gulp-uglify')

# Image plugins
imagemin       = require('gulp-imagemin')
# svgmin       = require('gulp-svgmin')

# General plugins
browserSync    = require('browser-sync')
reload         = browserSync.reload
gulpIf         = require("gulp-if")
watch          = require('gulp-watch')
gutil          = require('gulp-util')
path           = require('path')
notify         = require('gulp-notify')
mainBowerFiles = require('main-bower-files')
ignore         = require('gulp-ignore')
flatten        = require("gulp-flatten")
# browserify   = require('gulp-browserify')
# filter       = require('gulp-filter')

jsFilter = '**/*.js'
cssFilter = '**/**/**/*.{css,scss}'
fontFilter = ['**/*.{eot,svg,ttf,otf,woff}']

# Paths & Destinations
baseProjectDir = './app/'
baseProjectDest = './_dist/'
baseProjectDestSync = baseProjectDest+'**/**.**'
baseProductionDest = './_production/'

filePaths =
  fontsPath: baseProjectDir+'fonts/**/*.{eot,svg,ttf,otf,woff}'
  fontsDest: baseProjectDest+'fonts'
  componentsFontsPath: 'bower_components/**/*.{eot,svg,ttf,otf,woff}'
  templatesPath: baseProjectDir+'templates/**/**.jade'
  templatesDest: baseProjectDest
  coffeScriptPath: baseProjectDir+'coffee/*.coffee'
  coffeScriptDest: baseProjectDest+'js'
  componentsScriptPath: 'bower_components/**/*.{coffee,js}'
  sassPath: baseProjectDir+'sass/**/*.scss'
  sassDest: baseProjectDest+'css'
  componentSassPath: 'bower_components/**/**/**/'
  assetsPath: baseProjectDir+'assets/**/*.+(png|jpg|gif|jpeg)'
  assetsDest: baseProjectDest+'assets'

# process Fonts and return the stream.
gulp.task('fonts', () ->
  gulp.src([filePaths.fontsPath, filePaths.componentsFontsPath])
  .pipe(ignore.include(fontFilter))
  .pipe(flatten())
  .pipe(gulp.dest(filePaths.fontsDest))
  # Notify us that the task was completed
  .pipe(notify({ message: 'Fonts task complete' }))
)


# process JADE files and return the stream.
gulp.task 'templates', ->
  gulp.src(filePaths.templatesPath)
  .pipe(jade({pretty: true}))
  .pipe(flatten())
  .pipe(gulp.dest(filePaths.templatesDest))
  # Notify us that the task was completed
  .pipe(notify({ message: 'JADE templates task complete' }))

gulp.task 'vendor', ->
  gulp.src(mainBowerFiles())
  .pipe(gulpIf(/[.]coffee$/, coffee({
      bare: false
    })))
  .pipe(ignore.include(jsFilter))
  .pipe(concat 'vendor.js')
  .pipe(flatten())
  .pipe(gulp.dest(filePaths.coffeScriptDest))
  # Notify us that the task was completed
  .pipe(notify({ message: 'Vendor task complete' }))

gulp.task 'vendorCss', ->
  gulp.src(mainBowerFiles())
  .pipe(gulpIf( /[.]scss$/, compass(
    css: filePaths.sassDest
    sass: filePaths.componentSassPath) ))
  .pipe(ignore.include(cssFilter))
  .pipe(concat 'vendor.css')
  .pipe(flatten())
  .pipe(gulp.dest(filePaths.sassDest))
  # Notify us that the task was completed
  .pipe(notify({ message: 'Vendor CSS task complete' }))

# process CoffeScript files and return the stream with compiled js files.
gulp.task 'coffee', ->
  gulp.src(filePaths.coffeScriptPath)
  .pipe(coffee(bare: true).on('error', gutil.log))
  # Concatenate all JS files into one
  .pipe(concat('main.js'))
  # Minify JS
  # .pipe(uglify())
  .pipe(flatten())
  # Where to store the finalized JS
  .pipe(gulp.dest(filePaths.coffeScriptDest))
  # Notify us that the task was completed
  .pipe(notify({ message: 'CoffeScript task complete' }))


# process SASS files and return the stream with compiled js files.
gulp.task 'compass', ->
  gulp.src(filePaths.sassPath)
  .pipe(compass(
    css: filePaths.sassDest
    sass: './app/sass'))
  .pipe(flatten())
  .pipe(gulp.dest(filePaths.sassDest))
  # Notify us that the task was completed
  .pipe(notify({ message: 'Compass/SASS styles task complete' }))
# .pipe(rename(suffix: '.min')).pipe(minifycss()).pipe gulp.dest('html/css')

# Image tasks
gulp.task 'images', ->
  gulp.src(filePaths.assetsPath)
  .pipe(imagemin())
  .pipe(flatten())
  .pipe(gulp.dest(filePaths.assetsDest))
  # Notify us that the task was completed
  .pipe(notify({ message: 'Image task complete' }))

# Watch files for changes
gulp.task 'watch', [ 'browser-sync' ], ->
  # Watch HTML files
  gulp.watch filePaths.templatesPath, [ 'templates' ]
  # Watch SASS files
  gulp.watch filePaths.sassPath, [ 'compass' ]
  # Watch JS files
  gulp.watch filePaths.coffeScriptPath, [ 'coffee' ]
  # Watch image files
  gulp.watch filePaths.assetsPath, ['images']
  # # Watch SVG files
  # gulp.watch('src/images/vector/*', ['svgs']);
  return

gulp.task 'browser-sync', [
  'templates'
  'compass'
  'vendor'
  'vendorCss'
  'coffee'
  'images'
  'fonts'
], ->
  browserSync.init [ baseProjectDestSync ], server: baseDir: baseProjectDest
  return

gulp.task 'default', [
  'templates'
  'compass'
  'vendor'
  'vendorCss'
  'coffee'
  'images'
  'fonts'
  'watch'
  'browser-sync'
]